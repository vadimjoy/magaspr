$(function () {
    /**
     * Pages
     */
    var pages = [{
            class: "home",
            title: "HOME"
        },
        {
            class: "wwa",
            title: "WHO WE ARE"
        },
        {
            class: "wwc",
            title: "WHAT WE CAN"
        },
        {
            class: "oc",
            title: "OUR CLIENTS"
        },
        {
            class: "cu",
            title: "CONTACT US"
        }
    ];

    function getCurrentPage(index) {
        var key = index - 1;
        $.each(pages, function (key) {
            $(".js-page-nav").removeClass(pages[key].class)
        })
        $(".js-page-nav").addClass(pages[key].class).css('opacity', 1);
        $(".js-page-nav-title").html(pages[key].title);
    }

    $('#page').pagepiling({
        menu: '#nav',
        anchors: ['home', 'wwa', 'wwc', 'oc', 'cu'],
        sectionSelector: ".js-section",
        navigation: true,
        verticalCentered: false,
        onLeave: function (index, nextIndex) {
            $(".js-page-nav").css('opacity', 0);
        },
        afterLoad: function (anchorLink, index) {
            getCurrentPage(index);
        },
        afterRender: function () {
            getCurrentPage(1);
        }
    });

    $('.js-page-nav-prev').on('click', function () {
        $.fn.pagepiling.moveSectionUp();
    });

    $('.js-page-nav-next').on('click', function () {
        $.fn.pagepiling.moveSectionDown();
    });

    /**
     * Font-size
     */
    function calcFontSize(base, ow, oh) {
        var wmod = (ow - 70) / 3200,
            hmod = (oh - 70) / 2000,
            mod;

        if (wmod <= hmod) {
            mod = wmod;
        } else {
            mod = hmod
        }

        return base * mod;
    }

    $(window).on('resize', function (e) {
        var ow = e.target.outerWidth,
            oh = e.target.outerHeight,
            fsz;

        if (ow > 640) {
            fsz = calcFontSize(32, ow, oh) + 'px';
        } else {
            fsz = calcFontSize(76, ow, oh) + 'px';
        }

        $('html').css({
            'font-size': fsz
        });
    })

    $(window).trigger('resize');

    /**
     * Slider
     */
    new Swiper('.swiper-container', {
        effect: "fade",
        fadeEffect: {
            crossFade: true
        },
        pagination: {
            clickable: true,
            el: '.slider__pagination',
            bulletClass: 'slider__bullet',
            bulletActiveClass: 'slider__bullet_active'
        },
        autoplay: {
            delay: 2000,
        },
        loop: true
    })

    /**
     * Mobile menu
     */
    function showMobileMenu(close) {
        var $menu = $('.js-mobile-menu');

        if (close !== true) {
            $menu.hasClass('opened') ? closeMenu() : openMenu();
        } else {
            closeMenu();
        }

        function openMenu() {
            $('html, body').addClass('mobile-menu-opened');
            $menu.show().addClass('opened');
        }

        function closeMenu() {
            $('html, body').removeClass('mobile-menu-opened');
            $menu.hide().removeClass('opened');
        }
    }

    $('.js-burger').on('click', showMobileMenu);

    /**
     * Scroll to section
     */
    function scrollToSection() {
        var id = $(this).attr("href")
        showMobileMenu(true);
        $('html,body').animate({
            scrollTop: $(id).offset().top - 30
        }, 300)
    }

    $('.js-menu-link').on('click', scrollToSection);

    setTimeout(function () {
        $('body').css('opacity', 1);
    }, 300);
});